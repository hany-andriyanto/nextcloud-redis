**I. INSTALL NEXTCLOUD**
1. locate to folder nextcloud
2. run docker-compose by "docker-compose up -d"


**II. INSTALL MAILCOW**
1. locate to folder mailcow-docker
2. generate docker-compose.yml, read in folder 'mailcow-docker/step.txt'
3. run docker-compose by "docker-compose up -d"


**III. PROXYING WITH NGINX+LETSENCRYPT'S SSL**
1. locate to folder nginx-letsencrypt-proxy
2. run docker-compose by "docker-compose up -d"
3. change permission 'chmod +x init-le.sh'
4. create ssl by './init-le.ssh', fill input & run (ip must expose on your local network/LAN)
5. access by browser

