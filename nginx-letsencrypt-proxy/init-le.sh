#!/bin/bash
if ! [ -x "$(command -v docker-compose)" ]; then
  echo 'Error: docker-compose is not installed.' >&2
  exit 1
fi

#domains=(mydomain.com)
read -p "Input your domain: " domain
read -p "Input your email [SSL Config]: " your_email
read -p "Input proxied host (eg. 192.168.100.1 ): " proxied_host
read -p "Input proxied port (eg. 8080): " proxied_port

rsa_key_size=4096
data_path="./data/certbot"
email=${your_email}
staging=0 

create_config() {
#creating nginx proxy cache 
cat <<EOF > ./data/nginx/conf/$domain.conf
upstream $domain {
        server $proxied_host:$proxied_port;
}
server {
    listen 80;
    server_name $domain;
    root /usr/share/nginx/html;
    
    location /.well-known/acme-challenge/ {
    root /var/www/certbot;
   }

}

server {
   listen 80;
   listen 443 ssl;
EOF
cat <<\EOF >> ./data/nginx/conf/$domain.conf
   if ($scheme != "https") {
        return 301 https://$host$request_uri;
    }

EOF
cat <<EOF >> ./data/nginx/conf/$domain.conf
   ssl_certificate /etc/letsencrypt/live/$domain/fullchain.pem;
   ssl_certificate_key /etc/letsencrypt/live/$domain/privkey.pem;
   include /etc/letsencrypt/options-ssl-nginx.conf;
   ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;
EOF
cat <<\EOF >> ./data/nginx/conf/$domain.conf
    location / {
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        add_header Front-End-Https on;
        proxy_headers_hash_max_size 512;
        proxy_headers_hash_bucket_size 64;
        proxy_buffering off;
        proxy_redirect off;
        proxy_max_temp_file_size 0;
EOF
cat <<EOF >> ./data/nginx/conf/$domain.conf
        proxy_pass http://$domain; 
    }
}
EOF
}




if [ -d "$data_path" ]; then
  read -p "Existing data found for $domains. Continue and replace existing certificate? (y/N) " decision
  if [ "$decision" != "Y" ] && [ "$decision" != "y" ]; then
    exit
  fi
fi

if [ ! -e "$data_path/conf/options-ssl-nginx.conf" ] || [ ! -e "$data_path/conf/ssl-dhparams.pem" ]; then
  echo "### Downloading recommended TLS parameters ..."
  mkdir -p "$data_path/conf"
  curl -s https://raw.githubusercontent.com/certbot/certbot/master/certbot-nginx/certbot_nginx/_internal/tls_configs/options-ssl-nginx.conf > "$data_path/conf/options-ssl-nginx.conf"
  curl -s https://raw.githubusercontent.com/certbot/certbot/master/certbot/certbot/ssl-dhparams.pem > "$data_path/conf/ssl-dhparams.pem"
  echo
fi

echo "### Creating dummy certificate for $domains ..."
path="/etc/letsencrypt/live/$domains"
mkdir -p "$data_path/conf/live/$domains"
docker-compose run --rm --entrypoint "\
  openssl req -x509 -nodes -newkey rsa:1024 -days 1\
    -keyout '$path/privkey.pem' \
    -out '$path/fullchain.pem' \
    -subj '/CN=localhost'" certbot
echo
echo "### Starting nginx ..."
docker-compose up --force-recreate -d nginx
echo

echo "### Deleting dummy certificate for $domains ..."
docker-compose run --rm --entrypoint "\
  rm -Rf /etc/letsencrypt/live/$domains && \
  rm -Rf /etc/letsencrypt/archive/$domains && \
  rm -Rf /etc/letsencrypt/renewal/$domains.conf" certbot
echo

echo "### Requesting Let's Encrypt certificate for $domains ..."
#Join $domains to -d args
domain_args=""
for domain in "${domains[@]}"; do
    create_config
  domain_args="$domain_args -d $domain"
  done

# Select appropriate email arg
case "$email" in
  "") email_arg="--register-unsafely-without-email" ;;
  *) email_arg="--email $email" ;;
esac

# Enable staging mode if needed
if [ $staging != "0" ]; then staging_arg="--staging"; fi

docker-compose run --rm --entrypoint "\
  certbot certonly --webroot -w /var/www/certbot \
    $staging_arg \
    $email_arg \
    $domain_args \
    --rsa-key-size $rsa_key_size \
    --agree-tos \
    --force-renewal" certbot
echo

echo "### Reloading nginx ..."


docker-compose exec nginx nginx -s reload

wait
